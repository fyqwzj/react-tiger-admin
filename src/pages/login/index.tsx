import React, { useState } from 'react'
import { EyeInvisibleOutlined, EyeTwoTone, UnlockOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Form, Input, message, Row, Spin } from 'antd'
import { LoginInfo } from '../../typings/login'
import { RouteComponentProps, useHistory } from 'react-router-dom'
import { ProjectName, StoreKey } from '../../common/constants'
import { useStore } from '../../store'
import logo from '@/assets/logo.png'
import './login.less'
import { log } from 'console'

const Login: React.FC<RouteComponentProps> = () => {
  const [form] = Form.useForm<{ username: string; password: string }>()
  const [loading, setLoading] = useState(false)
  const { userStore } = useStore()
  const history = useHistory()
  const rules = {
    username: [
      { required: true, message: '请填写用户名', trigger: 'blur' },
      { min: 3, max: 12, message: '长度在 3 到 10 个字符', trigger: 'blur' },
    ],
    password: [
      { required: true, message: '请填写密码', trigger: 'blur' },
      { min: 3, max: 15, message: '长度在 3 到 10 个字符', trigger: 'blur' },
    ],
    vercode: [{ required: true, message: '请填写验证码', trigger: 'blur' }],
  }

  const { validateFields } = form
  // const onFinish = () => {
  //   validateFields()
  //     .then(async (values: LoginInfo) => {
  //       const params = Object.assign(values)
  //       setLoading(true)
  //       userStore.login(params).then(async (response) => {
  //         if (response) {
  //           if (response.authority_type === 'finance-internal' || response.authority_type === 'finance-external') {
  //             history.push('/payment')
  //           } else {
  //             history.push('/index')
  //           }
  //           // 延迟 1 秒显示欢迎信息
  //           setTimeout(() => {
  //             message.success('Hi, 欢迎回来')
  //           }, 1000)
  //         }
  //       }).catch((error: any) => {
  //         console.log(error, '000')
  //         if (error?.data?.detail) {
  //           message.error(error.data.detail)
  //         } else {
  //           message.error("登录失败请稍后再试")
  //         }
  //       }).finally(() => {
  //         setLoading(false)
  //       })
  //     })
  //     .catch((error: any) => {
  //     })
  // }

  const onFinish = () => {
    history.push('/index')
  }

  return (
    <div className="app-login">
      <Spin spinning={loading}>
        <Form
          className="app-login-form"
          layout="horizontal"
          form={form}
          onFinish={onFinish}
          initialValues={{
            username: '',
            password: '',
          }}
        >
          <h1>
            <img style={{ width: '60px' }} src={logo} alt={ProjectName} /> {ProjectName}
          </h1>
          <Form.Item name="username" rules={rules.username} hasFeedback>
            <Input size="large" autoComplete="off" prefix={<UserOutlined />} placeholder="用户名" />
          </Form.Item>
          <Form.Item name="password" rules={rules.password} hasFeedback>
            <Input.Password
              autoComplete="off"
              size="large"
              prefix={<UnlockOutlined />}
              placeholder="密码"
              iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
            />
          </Form.Item>
          <Row>
            <Button block size="large" type="primary" htmlType="submit" loading={loading}>登录</Button>
          </Row>
        </Form>
      </Spin>

    </div>
  )
}

export default Login
