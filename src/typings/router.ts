import React from 'react'

type ComponentType = React.ComponentType<any> & {name: string}

export interface IRouteBase {
  path: string
  title?: string
  icon?: string
  name?: string
  key?: string
  exact?: boolean
  meta?: IRouteMeta
  component?: ComponentType
  children?: Array<IRouteBase>
  // 302 跳转
  redirect?: string
  hidden?: boolean
}

export interface IRouteMeta {
  tabFixed?: boolean
  isCache?: boolean
  hidden?: boolean
  name: string
  icon: string
}

export interface IRoute extends IRouteBase {
  children?: IRoute[]
}

export interface MenuInfo {
  key: string;
  keyPath: string[];
  /** @deprecated This will not support in future. You should avoid to use this */
  item: React.ReactInstance;
  domEvent: React.MouseEvent<HTMLElement> | React.KeyboardEvent<HTMLElement>;
}
