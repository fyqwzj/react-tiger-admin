import { FC } from 'react'
import { Redirect, Switch, withRouter, Route } from 'react-router-dom'
import routes from '@/router'
import { Layout } from 'antd'
import { RouteComponentProps } from 'react-router'
import { IRoute } from '@/typings/router'
import { flatRouters } from '@/utils'

const { Content } = Layout

const AppContent: FC<RouteComponentProps> = () => {
  const menus = flatRouters(routes)

  return (
    <Content className="app-content">
      <Switch>
        <Redirect exact from="/" to="/home/index" />
        {menus.map((route: IRoute) => (
          <Route key={route.path} path={route.path} component={route.component} />
        ))}
        <Redirect to="/error/404" />
      </Switch>
    </Content>
  )
}

export default withRouter(AppContent)
