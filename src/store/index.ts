import { createContext, useContext } from 'react'
import appStore from './modules/app'
import userStore from './modules/user'

const store = {
  appStore,
  userStore,
}

const StoreContext = createContext(store)

export const useStore = () => useContext(StoreContext)

export default store
