import { db } from '@/utils/db'

export function useStorage() {
  return {
    set({ dbName = 'user', path, value, user = true }: any) {
      db.dbSet({ dbName, path, value, user })
    },
    get({ dbName = 'user', path, defaultValue = {}, user = true }: any) {
      return db.dbGet({ dbName, path, defaultValue, user })
    },
  }
}
