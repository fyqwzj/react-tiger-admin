import { IRoute } from '@/typings/router'
import loadable from '@loadable/component'

const Basic: Array<IRoute> = [
  {
    path: '/',
    title: '主页',
    name: 'index',
    exact: true,
    icon: 'HomeOutlined',
    component: loadable(() => import('@/pages/home')),
  }]

export default Basic
