import { lazy } from 'react'
import { IRoute } from '@/typings/router'
import Basic from './modules/basic'

const router: Array<IRoute> = [
  ...Basic,
  {
    path: '/error/404',
    title: '错误页面',
    name: 'Error',
    key: 'error',
    icon: 'FileUnknownOutlined',
    hidden: true,
    children: [
      {
        title: '404',
        path: '/error/404',
        name: '404',
        icon: 'FileTextOutlined',
        component: lazy(() => import('@/pages/error/404')),
      },
    ],
  },
]

export default router
