import request from '@/utils/request'
import qs from 'query-string'


export async function getTemplate(params: any) {
  return request({
    url: '/template',
    method: 'get',
    params,
  })
}


export async function deleteTemplate(params: any) {
  return request({
    url: '/template',
    method: 'delete',
    params,
  })
}


export async function downloadTemplate(data: any) {
  return request({
    url: '/download',
    method: 'get',
    responseType: 'blob',
    params: data,
  })
}

export async function updateTemplate(data: any) {
  return request({
    url: '/template',
    method: 'post',
    data
  })
}



// 获取当前登录用户信息
export async function fetchUserInfo(params: any) {
  return request({
    url: '/user/info',
    method: 'get',
    params,
  })
}

export function fetchLogin(data: any) {
  return request({
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
    url: 'login/access-token',
    method: 'post',
    data: qs.stringify(data)
  })
}
