import React, { FC } from 'react'
import { useStore } from '@/store'
import { observer } from 'mobx-react'
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons'
import './index.less'

const Hamburger: FC = () => {
  const { appStore } = useStore()

  const toggleSiderBar = () => {
    appStore.setCollapsed(!appStore.collapsed)
  }

  return (
    <div className="app-hamburger" onClick={toggleSiderBar}>
      { appStore.collapsed ? (<MenuUnfoldOutlined />) : (<MenuFoldOutlined />) }
    </div>
  )
}

export default observer(Hamburger)
