import React, { Fragment } from 'react'
import * as Icons from '@ant-design/icons'
import { isValidKey } from '@/utils'

type IProps = {
  [type: string]: string
}

export default function Icon(props: IProps) {
  const { type } = props
  let dynamicIcon = React.createElement('div')

  if (isValidKey(type, Icons)) {
    dynamicIcon = React.createElement(
      Icons[type],
      props,
    )
  }

  return (
    <>{dynamicIcon}</>
  )
}
